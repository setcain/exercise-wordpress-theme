<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <!-- styles -->
    <?php wp_head(); ?>
  </head>
  <body>

    <!-- header -->
    <header class="site-header">
      <div class="container">
        <!-- flex-container -->
        <div class="navigation-bar">
          
          <!-- logo -->
          <div class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="logo">
          </div>

          <!-- navigation bar -->
          <?php
            $args = array(
              // menu a agregar => nombre del menu en functions.php
              'theme_location' => 'main-menu',
              // etiqueta que encerrara los ul de la lista
              'container' => 'nav',
              // clase del container anteriormente creado
              'container_class' => 'main-menu'
            );
            wp_nav_menu($args);
          ?>
        
        </div>  
      </div>  
    </header>
