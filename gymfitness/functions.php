<?php

// menus de navegacion
function gymfitness_menus() {

    register_nav_menus(array(
        // arreglo (nombre del menu, text domain)
        'main-menu' => __('Menu principal', 'gymfitness-theme')
    ));

}

// hook init = se inicia wordpress y lanza la siguiente funcion
add_action('init', 'gymfitness_menus');

// scripts y styles
function gymfitness_scripts_styles() {

    // css
    wp_enqueue_style('normalize', get_template_directory_uri() . '/css/normalize.css', array(), '8.0.1');

    wp-wp_enqueue_style('slicknavCSS', get_template_directory_uri() . '/css/slicknav.min.css', array(), '1.0.0');

    wp_enqueue_style('googleFonts', 'https://fonts.googleapis.com/css?family=Open+Sans|Raleway:400,700,900|Staatliches&display=swap', array(), '1.0.0');

    wp_enqueue_style('style', get_stylesheet_uri(), array('normalize', 'googleFonts'), '1.0.0');

    // js
    wp_enqueue_script('slicknavJS', get_template_directory_uri() . '/js/jquery.slicknav.min.js', array('jquery'), '1.0.0', true);

    wp_enqueue_script('script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true);
}

// hook agregar scripts incluyendo styles
add_action('wp_enqueue_scripts', 'gymfitness_scripts_styles');

?>
